//
//  main.m
//  MangaFox Update
//
//  Created by Sugeng Wibowo on 2/12/14.
//  Copyright (c) 2014 SRIN Lab. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SRINAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SRINAppDelegate class]));
    }
}
