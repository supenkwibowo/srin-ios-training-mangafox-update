//
//  SRINUpdatesViewController.m
//  MangaFox Update
//
//  Created by Sugeng Wibowo on 2/13/14.
//  Copyright (c) 2014 SRIN Lab. All rights reserved.
//

#import "SRINUpdatesViewController.h"

@interface SRINUpdatesViewController () <UISearchBarDelegate>

@end

@implementation SRINUpdatesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
