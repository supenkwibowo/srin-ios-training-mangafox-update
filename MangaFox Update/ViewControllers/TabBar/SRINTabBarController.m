//
//  SRINTabBarController.m
//  MangaFox Update
//
//  Created by Sugeng Wibowo on 2/13/14.
//  Copyright (c) 2014 SRIN Lab. All rights reserved.
//

#import "SRINTabBarController.h"
#import "SRINUpdatesViewController.h"
#import "SRINFavouritesViewController.h"

@interface SRINTabBarController ()

@end

@implementation SRINTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"MangaFox Update";
    [self loadViewControllers];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods
- (void)loadViewControllers {
    SRINUpdatesViewController *updatesVC = [[SRINUpdatesViewController alloc] init];
    updatesVC.title = @"Updates";
    SRINFavouritesViewController *favVC = [[SRINFavouritesViewController alloc] init];
    favVC.title = @"Favourites";
    self.viewControllers = [NSArray arrayWithObjects:updatesVC, favVC, nil];
}

@end
